CREATE TABLE `t_order` (
  `id` bigint(20) NOT NULL,
  `name` varchar(120) DEFAULT NULL,
  `cnt` int(10) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `modify_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;