package com.lagou.mq.config;

import com.lagou.mq.bean.MessageBean;
import com.lagou.mq.bean.Order;
import com.lagou.mq.mapper.OrderMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Configuration
public class RabbitConfig {

    @Bean
    public Queue queue() {
        return new Queue("queue.wq");
    }

    @Bean
    public Exchange exchange() {
        return new DirectExchange("ex.wq");
    }

    @Bean
    public Binding binding() {
        return BindingBuilder
                .bind(queue())
                .to(exchange())
                .with("key.wq").noargs();
    }

    /**
     * 以下三个是死信队列
     * @return
     */
    @Bean
    public Exchange exchangeOrderTTL() {
        return new DirectExchange("ex.order_ttl");
    }

    @Bean
    public Queue queueOrderTTL() {
        return new Queue("queue.order_ttl");
    }

    @Bean
    public Binding bindingOrderTTL() {
        // ? 以下的key.dlx可以随便取?
        return BindingBuilder
                .bind(queueOrderTTL())
                .to(exchangeOrderTTL())
                .with("key.dlx").noargs();
    }

    /**
     * 以下三个是正常的队列
     * @return
     */
    @Bean
    public Exchange exchangeOrder() {
        return new DirectExchange("ex.order");
    }
    @Bean
    public Queue queueOrder() {
        Map<String, Object> props = new HashMap<>();
        props.put("x-message-ttl", 10000);
        props.put("x-dead-letter-exchange", "ex.order_ttl");
        props.put("x-dead-letter-routing-key", "key.dlx");
        return QueueBuilder.durable("queue.order")
                .withArguments(props)
                .build();
    }

    @Bean
    public Binding bindingOrder() {
        return BindingBuilder
                .bind(queueOrder())
                .to(exchangeOrder())
                .with("key.biz").noargs();
    }

    @Component
    class MqConsumer {

        private Logger log = LoggerFactory.getLogger(getClass());

        @Autowired
        @Lazy
        private OrderMapper orderMapper;

        @RabbitListener(queues = "queue.order_ttl")
        public void service(Long id) {
            Order item = this.orderMapper.selectById(id);
            if (item != null) {
                if (5 == item.getStatus()) {
                    log.info("已经确认:" + item.getId());
                    return;
                } else {
                    Order o = new Order();
                    o.setId(item.getId());
                    o.setStatus((short)3);
                    o.setModifyTime(new Date());
                    this.orderMapper.updateById(o);
                    log.info("自动取消:" + o.getId());
                }
            }
        }
    }


}
