package com.lagou.mq.bean;

import java.io.Serializable;

public class MessageBean implements Serializable {
    private static final long serialVersionUID = 4683332055447662401L;
    private Integer id;
    private String message;
    public MessageBean() {

    }

    public MessageBean(Integer id, String message) {
        this.id = id;
        this.message = message;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
