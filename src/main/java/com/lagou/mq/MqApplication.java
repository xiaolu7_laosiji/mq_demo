package com.lagou.mq;

import com.lagou.mq.bean.MessageBean;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class MqApplication {
    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(MqApplication.class, args);
    }
}
