package com.lagou.mq.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lagou.mq.bean.Order;
import com.lagou.mq.mapper.OrderMapper;
import org.springframework.amqp.core.MessageBuilder;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private OrderMapper orderMapper;

    @RequestMapping("/buy")
    public String buy(String name, int count) {
        Order info = new Order();
        info.setName(name);
        info.setCount(count);
        info.setStatus((short) 1);
        info.setModifyTime(new Date());
        this.orderMapper.insert(info);
        //byte[] data = (info.getId() + "").getBytes();
        this.rabbitTemplate.convertAndSend("ex.order", "key.biz", info.getId());

        return "success";
    }

    @RequestMapping("/pay")
    public String pay(Long id) {
        Order item = new Order();
        item.setStatus((short)5);
        item.setId(id);
        this.orderMapper.updateById(item);
        return "success";
    }

    @RequestMapping("/list")
    public List list(@RequestParam(required = false, defaultValue = "-1") Short status) {
        QueryWrapper wrapper = new QueryWrapper<>();
        if (status != null && status > 0) {
            wrapper.eq("status", status);
        }
        return this.orderMapper.selectList(wrapper);
    }
}
